<?php

namespace App\Service;

use App\Repository\OrganizationParameterAdminFieldRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ImportCsvService
 * @package App\Service
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CsvService
{
    private $app_inputcsv;
    private $kernelProjectDir;
    /**
     * @var OrganizationParameterAdminFieldRepository
     */
    private $organizationParameterAdminFieldRepository;


    /**
     * ImportCsv constructor.
     * @param $kernelProjectDir
     * @param $app_inputcsv
     * @param OrganizationParameterAdminFieldRepository $organizationParameterAdminFieldRepository
     */
    public function __construct($kernelProjectDir, $app_inputcsv,
                                OrganizationParameterAdminFieldRepository $organizationParameterAdminFieldRepository)
    {
        $this->app_inputcsv = $app_inputcsv;
        $this->kernelProjectDir = $kernelProjectDir;
        $this->organizationParameterAdminFieldRepository = $organizationParameterAdminFieldRepository;
    }

    /**
     * @param $filenameCsv
     * @return array
     */
    public function importCsvToPhp($filenameCsv)
    {

        $data = [];
        $csv_path = file($this->kernelProjectDir . '/public/' . $this->app_inputcsv . '/' . $filenameCsv . '.csv');

        $keys = str_getcsv(array_shift($csv_path), ';');
        foreach ($csv_path as $csvRecord) {
            // combine our $csvRecord with $keys array
            $data[] = array_combine($keys, str_getcsv($csvRecord, ';'));
        }

        return $data;
    }

    /**
     * Export d'une collection vers fichier format CSV
     * @param string $parameterType
     * @param array $collection
     * @param string $pathFolder
     * @param string $filename
     * @param string|null $textStringTop
     * @return string
     */
    public function exportToCsv(string $parameterType, array $collection, string $pathFolder, string $filename,
                                string $textStringTop = null)
    {

        $sep = ';';

        //on crée le dossier de destination si inexistant
        if (!is_dir($pathFolder)) {
            mkdir($pathFolder);
        }

        // *** Fichier + ligne d'entête des KO ***
        $filename .= '_' . date('YmdHis') . '.csv';
        $completePath = $pathFolder . '/' . $filename;

//        try {
        //si ligne documentation top fichier
        if ($textStringTop !== null) {
            $stringTop = $textStringTop . PHP_EOL . "Traitement d'export CSV le " . date('d/m/Y') . " à " . date('H:i:s') . PHP_EOL;
            file_put_contents($completePath, utf8_decode($stringTop));
        }

        //ECRITURE LIGNE D'ENTETE
        //A partir des champs elligibles export CSV de la collection, constituer la ligne d'entête
        $stringHeader = '';
        $obj = $this->organizationParameterAdminFieldRepository->findByKeyParameter($parameterType, 'csv');
        foreach ($obj as $o) {
            $stringHeader .= $o->getLabel() . $sep;
        }
        $stringHeader = substr($stringHeader, 0, -1);
        $stringHeader += PHP_EOL;
        file_put_contents($completePath, utf8_decode($stringHeader), FILE_APPEND);

        return true;
    }
}


//            //Parcours de la  pour écriture des données
//            $stringData = '';
//            foreach ($collection as $c) {

//                $stringData += PHP_EOL;
//                $stringData = mb_convert_encoding($stringData, 'UTF-8', 'ISO-8859-1');
//                file_put_contents($completePath, utf8_decode($stringData), FILE_APPEND);
//            }
//
//            $resultService['filenameCsv'] = $completePath;
//
//        } catch (\Exception $e) {
//            $resultService["errorMessage"] = "Anomalie rencontrée lors de la génération du fichier CSV : "
//                . $e . "\n" . "Veuillez contacter l'administrateur système.";
//            $this->logger->error("[CsvService/getPdf] - " . $resultService["errorMessage"]);
//        }
//
//        return $resultService;
//    }
//}