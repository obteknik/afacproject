<?php

namespace App\Command;

use App\Entity\Organization;
use App\Entity\User;
use App\Manager\OrganizationManager;
use App\Manager\UserManager;
use App\Repository\UserRepository;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * User Command Class that create the admin user.
 *     $ php bin/console app:create-admin-user
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CreateOrganizationCommand extends Command
{

    /**
     * @var OrganizationManager
     */
    private $organizationManager;

    /**
     * CreateAdminUserCommand constructor.
     * @param UserRepository $userRepository
     * @param OrganizationManager $organizationManager
     */
    public function __construct(UserRepository $userRepository, OrganizationManager $organizationManager)
    {
        parent::__construct();
        $this->organizationManager = $organizationManager;
    }

    protected static $defaultName = 'app:create-organization';

    protected function configure()
    {
        $this
            ->setDescription('Create the organization')
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $resultManager = $this->organizationManager->create("L'A.F.A.C de l'être",
            "07.81.34.06.44", "109 rue de la Louvetière", "77570", "CHATEAU-LANDON",
            "99999999900032", Organization::ORGANIZATION_SALETYPE_SERVICES);

        if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof Exception) {
            $io->warning($resultManager["errorMessage"]);

        } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof Organization) {

            $io->success('Great ! Organization created : ' . 'Nom: ' . $resultManager["item"]->getName());

            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }

}
