<?php

namespace App\Command;

use App\Entity\Discipline;
use App\Entity\Page;
use App\Manager\OrganizationManager;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * User Command Class that create the admin user.
 *     $ php bin/console app:create-admin-user
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CreateOrganizationParameterCommand extends Command
{

    /**
     * @var OrganizationManager
     */
    private $organizationManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CreateAdminUserCommand constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'app:create-organization-parameter';

    protected function configure()
    {
        $this
            ->setDescription('Create the organization parameters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //DISCIPLINES
        $disciplines = ['Médiation animale', 'Sport handicap', 'Massage bien-être',
            'Sophro psycho', 'Qi gong'];

        foreach ($disciplines as $d){
            $discipline = new Discipline();
            $discipline->setName($d);

            $this->entityManager->persist($discipline);
            $io->success('Great ! Organization parameter created : ' . 'Nom: ' . $discipline->getName());
        }

        //PAGES
        $pages = [
            ['title' => 'Politique de confidentialité',
            'content' => '<h1 style="color:blue">Politique de confidentialité</h1>'],
            ['title' => 'Accompagnement',
                'content' => '<h1 style="color:blue">Accompagnement</h1>'],
            ['title' => 'Formation',
                'content' => '<h1 style="color:blue">Formation</h1>'],
            'privacy' => '<h1 style="color:blue">Animation</h1>',
            ['title' => 'Coordination',
                'content' => '<h1 style="color:blue">Coordination</h1>'],
        ];

        foreach ($pages as $p) {
            $page = new Page();
            $page->setTitle($p['title']);
            $page->setContent($p['content']);

            $this->entityManager->persist($page);
            $io->success('Great ! Organization parameter created : ' . 'Nom: ' . $page->getTitle());
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }

}
