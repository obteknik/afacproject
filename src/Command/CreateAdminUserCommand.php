<?php

namespace App\Command;

use App\Entity\User;
use App\Manager\UserManager;
use App\Repository\UserRepository;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * User Command Class that create the admin user.
 *     $ php bin/console app:create-admin-user
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CreateAdminUserCommand extends Command
{
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * CreateAdminUserCommand constructor.
     * @param UserRepository $userRepository
     * @param UserManager $userManager
     */
    public function __construct(UserRepository $userRepository, UserManager $userManager)
    {
        parent::__construct();
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
    }

    protected static $defaultName = 'app:create-admin-user';

    protected function configure()
    {
        $this
            ->setDescription('Create the admin user')
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //je vérifie qu'un user ne possède pas déjà l'email 'admin@afac-etre.fr'
        $emailAdmin = 'admin@afac-etre.fr';
        $adminUser = $this->userRepository->findOneBy(['email' => $emailAdmin]);
        if($adminUser){
            $io->warning('Cet email existe déjà en base');
        } else {
            $adminUser = new User();
            $resultManager = $this->userManager->create(
                $adminUser, 'Olivier', 'BRASSART',$emailAdmin,
                'test', ['ROLE_ADMIN']);

            if (isset($resultManager["errorMessage"]) && $resultManager["errorMessage"] instanceof Exception) {
                $io->warning($resultManager["errorMessage"]);

            } elseif (isset($resultManager["item"]) && $resultManager["item"] instanceof User) {

                $io->success('Great ! Admin user created : '.
                    'Prénom: '.$adminUser->getFirstname().' - '.
                    'Nom :'.$adminUser->getLastname().' - '.
                    'Email :'.$adminUser->getEmail());

                return Command::SUCCESS;
            }

            return Command::FAILURE;
        }

    }
}
