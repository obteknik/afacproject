<?php

namespace App\Manager;

use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class OrganizationManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class OrganizationManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * @param string $name
     * @param string $phoneNumber
     * @param string $street
     * @param string $zipcode
     * @param string $city
     * @param string $siretNumber
     * @param string $saleType
     * @return array
     */
    public function create(string $name, string $phoneNumber, string $street, string $zipcode,
                           string $city, string $siretNumber, string $saleType)
    {

        try {
            $organization = new Organization();
            $organization->setName($name);
            $organization->setPhoneNumber($phoneNumber);
            $organization->setSiretNumber($siretNumber);
            $organization->setStreet($street);
            $organization->setZipcode($zipcode);
            $organization->setCity($city);
            $organization->setSaleType($saleType);

            $this->manager->persist($organization);
            $this->manager->flush();

            $resultManager["item"] = $user;


        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de l'utilisateur : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[UserManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}