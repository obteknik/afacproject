<?php

namespace App\Manager;

use App\Entity\Organization;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserManager
 * @package App\Manager
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class UserManager
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /*
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param User $user
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $plainPassword
     * @param array $roles
     * @param bool $agreedTerms
     * @return array
     */
    public function create(User $user, string $firstname, string $lastname, string $email, string $plainPassword,
                           $roles = [], bool $agreedTerms = true)
    {

        try {
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());
            $user->setFirstname($firstname);
            $user->setLastname(strtoupper($lastname));
            $user->setEmail($email);

            //password
            $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));

            //agree terms
            if (true === $agreedTerms) {
                $user->agreeTerms();
            }

            //roles
            $user->setRoles($roles);

            $this->manager->persist($user);
            $this->manager->flush();

            $resultManager["item"] = $user;


        } catch (\Exception $e) {
            $resultManager["errorMessage"] = "Anomalie rencontrée lors de la création de l'utilisateur : "
                . $e . "\n" . "Veuillez contacter l'administrateur système.";
            $this->logger->error("[UserManager/create] - " . $resultManager["errorMessage"]);
        }

        return $resultManager;
    }

}