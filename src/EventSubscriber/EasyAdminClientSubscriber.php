<?php

namespace App\EventSubscriber;

use App\Entity\Client;
use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Repository\ClientRepository;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;


/**
 * Class EasyAdminClientSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminClientSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;


    /**
     * EasyAdminClientSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param ClientRepository $clientRepository
     */
    public function __construct(OrganizationRepository $organizationRepository, ClientRepository $clientRepository)
    {
        $this->organizationRepository = $organizationRepository;
        $this->clientRepository = $clientRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setOrganization'], ['setCreatedAt'], ['setReference']],
        ];
    }

    public function setOrganization(BeforeEntityPersistedEvent $event)
    {
        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Client)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Client)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Client)) {
            return;
        }

        //recherche du dernier client créé pour l'organization (actt 1 seule organisation => dernier ID)
        $lastClient = $this->clientRepository->findOneBy([], ['id' => 'DESC']);

        if (!$lastClient) {
            $nextClientReference = 'C000001';
        } else {
            $lastReference = $lastClient->getReference();
            $lastChrono = substr($lastReference, -6);
            $nextClientReference = 'C' . sprintf("%06d", $lastChrono + 1);
        }

        $entity->setReference(strtoupper($nextClientReference));
    }

}