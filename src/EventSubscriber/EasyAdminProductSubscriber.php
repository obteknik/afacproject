<?php

namespace App\EventSubscriber;

use App\Entity\Product;
use App\Repository\ProductRepository;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminProductSubscriber implements EventSubscriberInterface
{

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * EasyAdminInvoiceSubscriber constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setReference'], ['setSlug'],],

        ];
    }

    public function setSlug(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        return Urlizer::urlize($entity->getName());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        $lastProduct = $this->productRepository->findOneBy([], ['id' => 'DESC']);

        if (!$lastProduct) {
            $nextProductReference = 'P000001';
        } else {
            $lastChrono = substr($lastProduct->getReference(), -6);
            $nextProductReference = 'P' . sprintf("%06d", $lastChrono + 1);
        }

        $entity->setReference($nextProductReference);
    }
}
