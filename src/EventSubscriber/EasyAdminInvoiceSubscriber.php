<?php

namespace App\EventSubscriber;

use App\Entity\Invoice;
use App\Entity\InvoiceDetail;
use App\Repository\InvoiceRepository;
use App\Repository\OrganizationRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;


/**
 * Class EasyAdminInvoiceSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminInvoiceSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var AdminContext|AdminContextProvider
     */
    private $context;

    /**
     * EasyAdminInvoiceSubscriber constructor.
     * @param OrganizationRepository $organizationRepository
     * @param InvoiceRepository $invoiceRepository
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @param AdminContextProvider $context
     */
    public function __construct(OrganizationRepository $organizationRepository,
                                InvoiceRepository $invoiceRepository,
                                ProductRepository $productRepository,
                                EntityManagerInterface $entityManager,
                                AdminContextProvider $context)
    {
        $this->organizationRepository = $organizationRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->context = $context;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => [['setOrganization'], ['setCreatedAt'], ['setReference']],
            AfterEntityPersistedEvent::class => [['addProduct']]
        ];
    }

    public function setOrganization(BeforeEntityPersistedEvent $event)
    {

        $organization = $this->organizationRepository->find(1);
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setOrganization($organization);
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());
    }

    public function setReference(BeforeEntityPersistedEvent $event)
    {

        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }

        //recherche de la dernière facture créée pour l'organization (actt 1 seule organisation => dernier ID)
        $lastInvoice = $this->invoiceRepository->findOneBy([], ['id' => 'DESC']);

        if (!$lastInvoice) {
            $nextInvoiceReference = 'F000001';
        } else {
            $lastChrono = substr($lastInvoice->getReference(), -6);
            $nextInvoiceReference = 'F' . sprintf("%06d", $lastChrono + 1);
        }

        $entity->setReference(strtoupper($nextInvoiceReference));
    }

    public function addProduct(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof Invoice)) {
            return;
        }
        $products = $this->context->getContext()->getRequest()->request->all()['Invoice']['products'];

        $products = array_values($products);
        $arrIdProduct = [];
        foreach ($products as $product) {
            foreach ($product as $p) {
                $arrIdProduct[] = $p;
            }
        }

        //Pour chaque id Product je crée les enregistrements de lignes de détails facture
        foreach ($arrIdProduct as $idProduct) {

            //recherche du prix unitaire du produit
            $invoice = $this->invoiceRepository->find($entity->getId());
            $product = $this->productRepository->find($idProduct);
            $price = $product->getPrice();

            $invoiceDetail = new InvoiceDetail();
            $invoiceDetail->setCreatedAt(new \DateTime());
            $invoiceDetail->setInvoice($invoice);
            $invoiceDetail->setProduct($product);
            $invoiceDetail->setQuantity(1);
            $invoiceDetail->setAmountHT($price);
            $this->entityManager->persist($invoiceDetail);
            $this->entityManager->flush();
        }
    }

}