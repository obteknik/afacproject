<?php

namespace App\EventSubscriber;

use App\Entity\Organization;
use App\Entity\Person;
use App\Entity\Slider;
use App\Entity\Post;
use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Gedmo\Sluggable\Util\Urlizer;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class EasyAdminSubscriber
 * @package App\EventSubscriber
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class EasyAdminSubscriber implements EventSubscriberInterface
{

    private $appKernel;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * EasyAdminSubscriber constructor.
     * @param KernelInterface $appKernel
     * @param ContainerInterface $container
     */
    public function __construct(KernelInterface $appKernel, ContainerInterface $container)
    {
        $this->appKernel = $appKernel;
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * The code must not depend on runtime state as it will only be called at compile time.
     * All logic depending on runtime state must be put into the individual methods handling the events.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
            BeforeEntityPersistedEvent::class => ['setIllustration'], //uniquement à la création (persist) !!!
            BeforeEntityUpdatedEvent::class => ['updateIllustration'] //pour la modification
        ];
    }

    public function uploadIllustration($event, $entityName){
        /** @var Product $entity */
        $entity = $event->getEntityInstance();

//        dd($_FILES);

        $originalName = pathinfo($_FILES[$entityName]['name']['illustration'],PATHINFO_FILENAME);
        $extension = pathinfo($_FILES[$entityName]['name']['illustration'], PATHINFO_EXTENSION);
        $tmp_name = $_FILES[$entityName]['tmp_name']['illustration'];
        $filename = Urlizer::urlize($originalName).'_'.uniqid();

        $project_dir = $this->appKernel->getProjectDir();

//        $originalNewFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
//        $newFilename = Urlizer::urlize($originalNewFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
//        $imageSize = $uploadedFile->getSize();

        move_uploaded_file($tmp_name, $project_dir.'/public/uploads/'.$filename.'.'.$extension);

        $entity->setIllustration($filename.'.'.$extension);
    }

    public function updateIllustration(BeforeEntityUpdatedEvent $event){

        if(!$event->getEntityInstance() instanceof Product
            && !$event->getEntityInstance() instanceof Organization
            && !$event->getEntityInstance() instanceof Person
            && !$event->getEntityInstance() instanceof Slider
            && !$event->getEntityInstance() instanceof Post){
            return;
        }

        $reflexion = new \ReflectionClass($event->getEntityInstance());
        $entityName = $reflexion->getShortName();

        if(isset($_FILES[$entityName])){
            if($_FILES[$entityName]['tmp_name']['illustration'] !== ''){
                $this->uploadIllustration($event, $entityName);
            }
        }

    }

    public function setIllustration(BeforeEntityPersistedEvent $event){
        if(!$event->getEntityInstance() instanceof Product
            && !$event->getEntityInstance() instanceof Organization
            && !$event->getEntityInstance() instanceof Person
            && !$event->getEntityInstance() instanceof Slider
            && !$event->getEntityInstance() instanceof Post){
            return;
        }

        $reflexion = new \ReflectionClass($event->getEntityInstance());
        $entityName = $reflexion->getShortName();

        $this->uploadIllustration($event, $entityName);
    }
}