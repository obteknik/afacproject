<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Repository\ProductRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ProductCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class ProductCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductCrudController constructor.
     * @param TranslatorInterface $translator
     * @param ProductRepository $productRepository
     */
    public function __construct(TranslatorInterface $translator, ProductRepository $productRepository)
    {
        $this->translator = $translator;
        $this->productRepository = $productRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-tag"></i> ' . $this->translator->trans('admin.product.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-tag"></i> ' . $this->translator->trans('admin.product.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-tag"></i> ' . $this->translator->trans('admin.product.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-tag"></i> ' . $this->translator->trans('admin.product.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.product.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.product.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.product.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        $lastProduct = $this->productRepository->findOneBy([],['id' => 'DESC']);

        if(!$lastProduct){
            $nextProductReference = 'P000001';
        } else {
            $lastChrono = substr($lastProduct->getReference(),-6);
            $nextProductReference = 'P' . sprintf("%06d", $lastChrono + 1);
        }

        return [
            TextField::new('reference', 'Référence du produit')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Référence du produit...', 'disabled' => true],
                'data' => $nextProductReference
            ]),
            AssociationField::new('category', 'Catégorie')->setFormTypeOptions([
                'placeholder' => 'Catégorie du produit...',
            ]),
            TextField::new('name', 'Nom du produit')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Nom du produit...', 'autofocus' => true]
            ]),
//            SlugField::new('slug')->setTargetFieldName('name')
//                ->setFormTypeOptions([
//                    'attr' => ['placeholder' => 'Slug du produit...']
//                ]),
            ImageField::new('illustration', 'Image')
//                ->setBasePath('uploads/')->setFormTypeOptions(['data_class' =>null]),
                ->setBasePath('uploads/')->setFormTypeOptions(['mapped' => false, 'required' => false]),

            TextField::new('subtitle', 'Sous-titre')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Sous-tire du produit...']
            ]),
            TextareaField::new('description', 'Description')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Description du produit...']
            ]),
            BooleanField::new('isBest', 'En page d\'accueil'),
            MoneyField::new('price', 'Prix')->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => 'Prix du produit...']
                ]),
            IntegerField::new('stock')->setFormTypeOptions([
//                'disabled' => 'disabled'
            ]),
            BooleanField::new('isEnabled', 'Activé'),

        ];
    }

}
