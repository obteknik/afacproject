<?php

namespace App\Controller\Admin;

use App\Entity\Organization;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrganizationCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function index(AdminContext $context)
    {
        // redirect to edit organization
        /*
        {% set url = ea_url()
            .setDashboard('App\\Controller\\Admin\\DashboardController')
            .setController('App\\Controller\\Admin\\ClientCrudController')
            .setAction('detail') %}
        */
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        $crudId = '0d20ca2';
        $enityId = 1;
        $routeBuilder
            ->setDashboard('App\\Controller\\Admin\\DashboardController')
            ->setController('App\\Controller\\Admin\OrganizationCrudController')
            ->setAction('detail')
            ->setCrudId($crudId)
            ->setEntityId($enityId);

        return $this->redirect($routeBuilder->generateUrl());
    }

    public static function getEntityFqcn(): string
    {
        return Organization::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->disable('new', 'delete')
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud
            ->setPageTitle('index', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.index.headTitle',
                    [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-home"></i> ' . $this->translator->trans('admin.organization.edit.headTitle',
                [], 'admin'));
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('saleType', "Types de vente")
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => 'Types de vente...']
                    ]
                )->setChoices([
                    Organization::ORGANIZATION_SALETYPE_GOODS => 0,
                    Organization::ORGANIZATION_SALETYPE_SERVICES => 1,
                    Organization::ORGANIZATION_SALETYPE_GOODS_SERVICES => 2,
                ]),
            TextField::new('name', 'Nom'),
            TextField::new('phoneNumber', 'Téléphone'),
            EmailField::new('email', 'Email'),
            TextField::new('street', 'Rue'),
            TextField::new('zipcode', 'Code postal'),
            TextField::new('city', 'Ville'),
            TextField::new('siretNumber', 'N° siret'),
            NumberField::new('taxRate'),
            ImageField::new('illustration', 'Logo')
//                ->setBasePath('uploads/')->setFormTypeOptions(['data_class' =>null]),
                ->setBasePath('uploads/')->setFormTypeOptions(['mapped' => false, 'required' => false]),

        ];
    }
}
