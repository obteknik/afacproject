<?php

namespace App\Controller\Admin;

use App\Entity\Carrier;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CarrierCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CarrierCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CarrierCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Carrier::class;
    }

    public function configureAssets(Assets $assets): Assets
    {
        return $assets->addCssFile('assets/css/admin.css');
    }

    public function configureActions(Actions $actions): Actions
    {
        // in PHP 7.4 and newer you can use arrow functions
        // ->update(Crud::PAGE_INDEX, Action::NEW,
        //     fn (Action $action) => $action->setIcon('fa fa-file-alt')->setLabel(false))

        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.carrier.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', $this->translator->trans('admin.carrier.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail',$this->translator->trans('admin.carrier.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new',$this->translator->trans('admin.carrier.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit',$this->translator->trans('admin.carrier.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', $this->translator->trans('admin.carrier.form.name.label',
                [], 'admin'))->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => 'admin.carrier.form.name.placeholder',
                        'autofocus' =>true]
                ]
            ),
            TextareaField::new('description', $this->translator->trans('admin.carrier.form.description.label',
                [], 'admin'))->setFormTypeOptions(
                [
                    'attr' => ['placeholder' => $this->translator->trans('admin.carrier.form.description.placeholder',
                        [], 'admin')]
                ]
            ),
            MoneyField::new('price', $this->translator->trans('admin.carrier.form.price.label',
                [], 'admin'))
                ->setCurrency('EUR')
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => '0,00']
                    ]
                ),
        ];
    }

}
