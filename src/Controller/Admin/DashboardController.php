<?php

namespace App\Controller\Admin;

use App\Entity\Carrier;
use App\Entity\Category;
use App\Entity\Client;
use App\Entity\Discipline;
use App\Entity\Invoice;
use App\Entity\Person;
use App\Entity\Quote;
use App\Entity\Slider;
use App\Entity\Order;
use App\Entity\Organization;
use App\Entity\Page;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Stock;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class DashboardController extends AbstractDashboardController
{
    private $params;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(ParameterBagInterface $params, TranslatorInterface $translator)
    {
        $this->params = $params;
        $this->translator = $translator;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        // *** Homepage Dashboard ***
        return parent::index();
        // redirect to some CRUD controller
//        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
//        return $this->redirect($routeBuilder->setController(OrderCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->params->get('app.name'))
            ->setTranslationDomain('admin');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('assets/css/admin.css')
            ->addJsFile('assets/js/form-type-collection.js')
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Site vitrine', 'fas fa-home', 'home');
        yield MenuItem::linktoRoute('Mon agenda', 'fas fa-calandar', 'agenda');
        yield MenuItem::linktoDashboard($this->translator->trans('admin.sidebar.menu.dashboard.label',
            [], 'admin'), 'fas fa-chart-pie');

        yield MenuItem::subMenu('Sécurité', 'fa fa-lock')->setSubItems([
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.information.label',
                [], 'admin'), 'fa fa-key', Organization::class),

            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.user.label',
                [], 'admin'), 'fa fa-users', User::class)
        ]);

        yield MenuItem::subMenu('Client & comptabilité', 'fa fa-users')->setSubItems([
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.client.label',
                [], 'admin'), 'fa fa-users', Client::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.quote.label',
                [], 'admin'), 'fa fa-file-pdf', Quote::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.invoice.label',
                [], 'admin'), 'fa fa-file-pdf', Invoice::class),
        ]);

        yield MenuItem::section('Intervenants & disciplines');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.person.label',
            [], 'admin'), 'fa fa-users', Person::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.discipline.label',
            [], 'admin'), 'fa fa-edit', Discipline::class);

        yield MenuItem::section('Commerce');
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.order.label',
            [], 'admin'), 'fa fa-shopping-cart', Order::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.category.label',
            [], 'admin'), 'fa fa-list', Category::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.product.label',
            [], 'admin'), 'fa fa-tag', Product::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.carrier.label',
            [], 'admin'), 'fa fa-truck', Carrier::class);
        yield MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.stock.label',
            [], 'admin'), 'fas fa-edit', Stock::class);

        yield MenuItem::section('Conception');

        yield MenuItem::subMenu('Conception', 'fa fa-edit')->setSubItems([
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.slider.label',
                [], 'admin'), 'fa fa-desktop', Slider::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.page.label',
                [], 'admin'), 'fas fa-file', Page::class),
            MenuItem::linkToCrud($this->translator->trans('admin.sidebar.menu.post.label',
                [], 'admin'), 'fas fa-edit', Post::class)
        ]);

    }
}
