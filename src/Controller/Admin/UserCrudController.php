<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class UserCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' .$this->translator->trans('admin.user.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.user.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.user.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.user.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstname', 'Prénom')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Prénom de l\'utilisateur...','autofocus' =>true]
            ]),
            TextField::new('lastname', 'Nom')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Nom de l\'utilisateur...']
            ]),
            EmailField::new('email', 'Email')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Adresse email de l\'utilisateur...']
            ]),
            TextField::new('password', 'Mot de passe')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Adresse email de l\'utilisateur...']
            ]),
            ChoiceField::new('hasRoleAdmin', "Rôle")
                ->setFormTypeOptions(
                    [
                        'attr' => ['placeholder' => 'Rôle...']
                    ]
                )->setChoices([
                    User::USER_ROLE_USER => 0,
                    User::USER_ROLE_ADMIN => 1,
                ]),
        ];
    }

}
