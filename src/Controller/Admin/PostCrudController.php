<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Post;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class PostCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Post::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
        $crud->setPageTitle('index', '<i class="fa fa-edit"></i> ' . $this->translator->trans('post.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail','<i class="fa fa-edit"></i> ' . $this->translator->trans('post.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new','<i class="fa fa-edit"></i> ' . $this->translator->trans('post.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit','<i class="fa fa-edit"></i> ' . $this->translator->trans('post.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.post.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.post.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.post.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre du post')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Titre...', 'autofocus' =>true]
            ]),
            AdminCkEditorField::new('content', 'Contenu de l\'article')
                ->setCustomOptions(
                    [
                        'config' => ['placeholder' => 'Contenu de l\'article...']
                    ]
                ),
            ImageField::new('illustration', 'Image')
                ->setBasePath('uploads/')->setFormTypeOptions(['mapped' =>false, 'required' => false]),
            TextField::new('illustrationAlt', 'Balise Alt illustration')->setFormTypeOptions([
                'attr' => ['placeholder' => 'Balise Alt illustration...']
            ]),
            BooleanField::new('isEnabled', 'Activer'),
            BooleanField::new('isTextRight', 'Texte à droite'),
        ];
    }

}
