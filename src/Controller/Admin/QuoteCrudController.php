<?php

namespace App\Controller\Admin;

use App\Entity\Quote;
use App\Services\PdfSnappyService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class QuoteCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var PdfSnappyService
     */
    private $snappy;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CarrierCrudController constructor.
     * @param TranslatorInterface $translator
     * @param PdfSnappyService $snappy
     * @param LoggerInterface $logger
     */
    public function __construct(TranslatorInterface $translator,PdfSnappyService $snappy, LoggerInterface $logger)
    {
        $this->translator = $translator;
        $this->snappy = $snappy;
        $this->logger = $logger;
    }

    public static function getEntityFqcn(): string
    {
        return Quote::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        // in PHP 7.4 and newer you can use arrow functions
        // ->update(Crud::PAGE_INDEX, Action::NEW,
        //     fn (Action $action) => $action->setIcon('fa fa-file-alt')->setLabel(false))

        $viewQuotePDF = Action::new('viewQuotePDF', '','fas fa-file-pdf')
            ->linkToCrudAction('viewQuotePDF');
//            ->displayIf(static function ($entity) {
//                return $entity->isPublished();
//            }
//            )
        ;
        return $actions
            ->add(Crud::PAGE_INDEX, $viewQuotePDF)
            ->reorder(Crud::PAGE_INDEX, [Action::EDIT, Action::DELETE, 'View Quote'])
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.quote.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.quote.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.quote.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    /**
     * Génération du devis en PDF
     * @param AdminContext $context
     * @return string
     */
    public  function viewQuotePDF(AdminContext $context){

        /** @var Quote $quote */
        $quote = $context->getEntity()->getInstance();

        $filename = 'Devis n° ' . $quote->getReference() . '_' . uniqid();
        $model = '_pdfModels/invoice.html.twig';

        //génération du pdf   //---- $organization->getLogo()
        $resultService = $this->snappy->getPdf($filename, 'app.outputpdf.quote.dir',
            'A4', 'Portrait', $model,
            ['organizationLogo' => null, 'title' => 'DEVIS N° ' . $quote->getReference()], 2);

        if (isset($resultService['pdf'])) {
            return new \Symfony\Component\HttpFoundation\Response($resultService['pdf'], 200,
                ['Content-Type' => 'application/pdf',
                    'Content-Disposition' => $resultService['outputType'] . '; filename=' . $resultService['filename'] . ".pdf"]
            );
        } else {

            $this->addFlash('error', "Anomalie rencontrée lors de la génération du fichier PDF.
            Contacter l'administrateur système");
            $this->logger->error("[QuoteCrudController/pdf] - " . $resultService["errorMessage"]);

            return $this->redirectToRoute('admin');
        }
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', $this->translator->trans('admin.quote.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail',$this->translator->trans('admin.quote.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new',$this->translator->trans('admin.quote.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit',$this->translator->trans('admin.quote.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt', $this->translator->trans('admin.quote.form.createdAt.label',
                [], 'admin')),
            TextField::new('reference', $this->translator->trans('admin.quote.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['autofocus' =>true, 'placeholder' => $this->translator->trans('admin.quote.form.reference.placeholder',
                    [], 'admin')]
            ]),
            MoneyField::new('amountHT', $this->translator->trans('admin.quote.form.amountHT.label',
                [], 'admin'))->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => $this->translator->trans('admin.quote.form.amountHT.placeholder',
                        [], 'admin')]
                ]),
            MoneyField::new('amountTTC', $this->translator->trans('admin.quote.form.amountTTC.label',
                [], 'admin'))->setCurrency('EUR')
                ->setFormTypeOptions([
                    'attr' => ['placeholder' => $this->translator->trans('admin.quote.form.amountTTC.placeholder',
                        [], 'admin')]
                ]),

        ];
    }
}
