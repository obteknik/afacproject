<?php

namespace App\Controller\Admin;

use App\Classe\AdminCkEditorField;
use App\Entity\Person;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class PersonCrudController
 * @package App\Controller\Admin
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class PersonCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Person::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

        $crud->setPageTitle('index', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.index.headTitle',
                [], 'admin'));
        $crud->setPageTitle('detail', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.detail.headTitle',
                [], 'admin'));
        $crud->setPageTitle('new', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.new.headTitle',
                [], 'admin'));
        $crud->setPageTitle('edit', '<i class="fa fa-user"></i> ' . $this->translator->trans('admin.person.edit.headTitle',
                [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.person.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.person.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.person.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstname', $this->translator->trans('admin.person.form.firstname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.person.form.firstname.placeholder',
                    [], 'admin'), 'autofocus' =>true]
            ]),
            TextField::new('lastname', $this->translator->trans('admin.person.form.lastname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.person.form.lastname.placeholder',
                    [], 'admin')]
            ]),
            TextField::new('address', $this->translator->trans('admin.person.form.address.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.person.form.address.placeholder',
                    [], 'admin')]
            ]),
            TextField::new('phoneNumber', $this->translator->trans('admin.person.form.phoneNumber.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.person.form.phoneNumber.placeholder',
                    [], 'admin')]
            ]),
            ImageField::new('illustration', $this->translator->trans('admin.person.form.illustration.label',
                [], 'admin'))
                ->setBasePath('uploads/')->setFormTypeOptions(['mapped' => false, 'required' => false]),
            TextareaField::new('description', $this->translator->trans('admin.person.form.description.label',
                [], 'admin'))
                ->setCustomOptions(
                    [
                        'config' => ['placeholder' => 'Contenu de la page...']
                    ]
                ),
            AssociationField::new('disciplines')

        ];
    }

}
