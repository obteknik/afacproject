<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Repository\ClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientCrudController extends AbstractCrudController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * CategoryCrudController constructor.
     * @param TranslatorInterface $translator
     * @param ClientRepository $clientRepository
     */
    public function __construct(TranslatorInterface $translator, ClientRepository $clientRepository)
    {
        $this->translator = $translator;
        $this->clientRepository = $clientRepository;
    }


    public static function getEntityFqcn(): string
    {
        return Client::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle('index', $this->translator->trans('admin.client.index.headTitle',
            [], 'admin'));
        $crud->setPageTitle('detail', $this->translator->trans('admin.client.detail.headTitle',
            [], 'admin'));
        $crud->setPageTitle('new', $this->translator->trans('admin.client.new.headTitle',
            [], 'admin'));
        $crud->setPageTitle('edit', $this->translator->trans('admin.client.edit.headTitle',
            [], 'admin'));
        $crud->setDefaultSort(['id' => 'ASC']);
        return $crud;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail')
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setIcon('fa fa-plus-circle')
                    ->setLabel($this->translator->trans('admin.client.index.button.add.label',
                        [], 'admin'))->setCssClass('action-new btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.client.index.button.save.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                return $action
                    ->setIcon('fa fa-save')
                    ->setLabel($this->translator->trans('admin.client.index.button.saveAndAnother.label',
                        [], 'admin'))->setCssClass('btn btn-info');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action
                    ->setIcon('fa fa-eye')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-edit')
                    ->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel(false);
            })
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        //référence client
        $lastClient = $this->clientRepository->findOneBy([], ['id' => 'DESC']);

        if (!$lastClient) {
            $nextClientReference = 'C000001';
        } else {
            $lastReference = $lastClient->getReference();
            $lastChrono = substr($lastReference, -6);

            $nextClientReference = 'C' . sprintf("%06d", $lastChrono + 1);
        }

        return [
            DateTimeField::new('createdAt', $this->translator->trans('admin.invoice.form.createdAt.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true,]
            ]),
            TextField::new('reference', $this->translator->trans('admin.invoice.form.reference.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['disabled' => true, 'placeholder' => $this->translator->trans('admin.invoice.form.reference.placeholder',
                    [], 'admin')],
                'data' => $nextClientReference,
            ]),
            TextField::new('companyName', $this->translator->trans('admin.client.form.companyName.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['autofocus' => true, 'placeholder' => $this->translator->trans('admin.client.form.companyName.placeholder',
                    [], 'admin')
                ]]),
            TextField::new('firstname', $this->translator->trans('admin.client.form.firstname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.firstname.placeholder',
                    [], 'admin')
                ]]),
            TextField::new('lastname', $this->translator->trans('admin.client.form.lastname.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.lastname.placeholder',
                    [], 'admin')
                ]]),
            TextField::new('address', $this->translator->trans('admin.client.form.address.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.address.placeholder',
                    [], 'admin')
                ]]),
            TelephoneField::new('phoneNumber', $this->translator->trans('admin.client.form.phoneNumber.label',
                [], 'admin'))->setFormTypeOptions([
                'attr' => ['placeholder' => $this->translator->trans('admin.client.form.phoneNumber.placeholder',
                    [], 'admin')
                ]]),
        ];
    }

}
