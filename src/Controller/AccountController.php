<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AccountController extends AbstractController
{
    /**
     * @Route({ "en": "/account", "fr": "/compte"}, name="account")
     */
    public function index(): Response
    {
        return $this->render('account/index.html.twig');
    }
}
