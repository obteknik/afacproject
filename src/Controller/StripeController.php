<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StripeController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class StripeController extends AbstractController
{
    /**
     * @var string
     */
    private $params;

    /**
     * StripeController constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/commande/create-session/{reference}", name="stripe_create_session")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param OrderRepository $orderRepository
     * @param ProductRepository $productRepository
     * @param $reference
     * @return Response
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function index(Request $request,EntityManagerInterface $entityManager,OrderRepository $orderRepository, ProductRepository $productRepository, $reference): Response
    {
        $products_for_stripe = [];
//        $MY_DOMAIN = 'http://127.0.0.1:8000';
        $MY_DOMAIN = $this->params->get('app.domain');
        $locale = $request->getLocale();

        /** @var Order $order */
        $order = $orderRepository->findOneByReference($reference);

        if(!$order){
            new JsonResponse(['error' => 'order']);
        }

        /** @var OrderDetails $product */
        foreach ($order->getOrderDetails()->getValues() as $product) {
            $product_object = $productRepository->findOneByName($product->getProduct());
            $products_for_stripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $product->getPrice(),
                    'product_data' => [
                        'name' => $product->getProduct(),
                        'images' => [$MY_DOMAIN . "/uploads/" . $product_object->getIllustration()],
                    ],
                ],
                'quantity' => $product->getQuantity(),
            ];
        }

        $products_for_stripe[] = [
            'price_data' => [
                'currency' => 'eur',
                'unit_amount' => $order->getCarrierPrice(),
                'product_data' => [
                    'name' => $order->getCarrierName(),
                    'images' => [$MY_DOMAIN], //rajouter une image collissimo...
                ],
            ],
            'quantity' => 1,
        ];

        //Initialisation Stripe
        Stripe::setApiKey($this->params->get('stripe.secret_key'));

        //Création d'une Checkout session
        $relativePathSuccess='';
        $relativePathCancel='';
        if($locale == 'en'){
            $relativePathSuccess =  '/order/thank/{CHECKOUT_SESSION_ID}';
            $relativePathCancel =  '/order/cancel/{CHECKOUT_SESSION_ID}';
        } else if($locale == 'fr') {
            $relativePathSuccess =  '/commande/merci/{CHECKOUT_SESSION_ID}';
            $relativePathCancel =  '/commande/erreur/{CHECKOUT_SESSION_ID}';
        }
        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [$products_for_stripe],
            'mode' => 'payment',
            'success_url' => $MY_DOMAIN .'/'.  $locale . $relativePathSuccess,
            'cancel_url' => $MY_DOMAIN .'/'.  $locale .  $relativePathCancel,
        ]);

        $order->setStripeSessionId($checkout_session->id);
        $entityManager->flush();

        $response = new JsonResponse(['id' => $checkout_session->id]);
        return $response;
    }
}
