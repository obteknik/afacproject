<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CartController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class CartController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CartController constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route({ "en": "/my-cart", "fr": "/mon-panier"}, name="cart")
     * @param Cart $cart
     * @return Response
     */
    public function index(Cart $cart): Response
    {

        return $this->render('cart/index.html.twig', [
            'cart' => $cart->getFull()
        ]);
    }

    /**
     * Ajout d'un article au panier
     * @Route("/cart/add/{id}", name="add_to_cart")
     * @param Request $request
     * @param Cart $cart
     * @param $id
     * @return Response
     */
    public function add(Request $request, Cart $cart, $id): Response
    {

        $cart->add($id);

        $referer = filter_var($request->headers->get('referer'), FILTER_SANITIZE_URL);
        $parseReferer = str_replace($request->getSchemeAndHttpHost(), '', $referer,);

        $product = $this->productRepository->find($id);
        if (!$product) {
            return $this->redirectToRoute('cart');
        }
        $urlProduct = $this->generateUrl('product', ['slug' => $product->getSlug()]);

        if ($parseReferer == $urlProduct) {
            return $this->redirectToRoute('product', ['slug' => $product->getSlug()]);
        }
        return $this->redirectToRoute('cart');
    }

    /**
     * Diminue de 1 la quantité d'article du panier
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     * @param Request $request
     * @param Cart $cart
     * @param $id
     * @return Response
     */
    public function decrease(Request $request, Cart $cart, $id): Response
    {
        $cart->decrease($cart, $id);

        $referer = filter_var($request->headers->get('referer'), FILTER_SANITIZE_URL);
        $parseReferer = str_replace($request->getSchemeAndHttpHost(), '', $referer,);

        $product = $this->productRepository->find($id);
        if (!$product) {
            return $this->redirectToRoute('cart');
        }
        $urlProduct = $this->generateUrl('product', ['slug' => $product->getSlug()]);

        if ($parseReferer == $urlProduct) {
            return $this->redirectToRoute('product', ['slug' => $product->getSlug()]);
        }

        return $this->redirectToRoute('cart');
    }

    /**
     * Supprime totalement un article du panier
     * @Route("/cart/delete/{id}", name="delete_to_cart")
     * @param Cart $cart
     * @param $id
     * @return Response
     */
    public function delete(Cart $cart, $id): Response
    {
        $cart->delete($id);
        return $this->redirectToRoute('cart');
    }

    /**
     * Vide tout le panier
     * @Route("/cart/remove", name="remove_my_cart")
     * @param Cart $cart
     * @return Response
     */
    public function remove(Cart $cart): Response
    {

        $cart->remove();

        return $this->redirectToRoute('products');
    }

    /**
     * Récupère le contenu HTML du panier pour la popover
     * @Route("/cart/content", name="cart_ajax", options={"expose":true})
     * @param Cart $cart
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function getHtmlCart(Cart $cart, TranslatorInterface $translator): Response
    {
        $cartHtml = $this->render('cart/contentAjax.html.twig', [
            'cart' => $cart->getFull()
        ]);

        $titleCard = $translator->trans('cart.headTitle');
        return new JsonResponse(['title' => $titleCard, 'content' => $cartHtml->getContent()], Response::HTTP_OK);
    }
}
