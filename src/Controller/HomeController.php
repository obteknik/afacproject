<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Slider;
use App\Entity\Post;
use App\Entity\Product;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class HomeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * HomeController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(): Response
    {
        $products = $this->entityManager->getRepository(Product::class)->findByIsBest(1);
        $sliders = $this->entityManager->getRepository(Slider::class)->findBy(['isEnabled' => true]);
        $posts = $this->entityManager->getRepository(Post::class)->findBy(['isEnabled' => true]);
        $persons = $this->entityManager->getRepository(Person::class)->findAll();

        return $this->render('home/index.html.twig', [
            'products' => $products,
            'sliders' => $sliders,
            'posts' => $posts,
            'persons' => $persons,
            'home' => true
        ]);
    }

    /**
     * @Route("/agenda", name="agenda")
     * @return Response
     */
    public function agenda(): Response
    {

        return $this->render('home/agenda.html.twig');
    }
}
