<?php

namespace App\Entity;

use App\Repository\QuoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuoteRepository::class)
 */
class Quote
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="float")
     */
    private $amountHT;

    /**
     * @ORM\Column(type="float")
     */
    private $amountTTC;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=QuoteDetail::class, mappedBy="quote")
     */
    private $quoteDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="quotes")
     */
    private $organization;

    public function __construct()
    {
        $this->quoteDetails = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getAmountHT(): ?float
    {
        return $this->amountHT;
    }

    public function setAmountHT(float $amountHT): self
    {
        $this->amountHT = $amountHT;

        return $this;
    }

    public function getAmountTTC(): ?float
    {
        return $this->amountTTC;
    }

    public function setAmountTTC(float $amountTTC): self
    {
        $this->amountTTC = $amountTTC;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|QuoteDetail[]
     */
    public function getQuoteDetails(): Collection
    {
        return $this->quoteDetails;
    }

    public function addQuoteDetail(QuoteDetail $quoteDetail): self
    {
        if (!$this->quoteDetails->contains($quoteDetail)) {
            $this->quoteDetails[] = $quoteDetail;
            $quoteDetail->setQuote($this);
        }

        return $this;
    }

    public function removeQuoteDetail(QuoteDetail $quoteDetail): self
    {
        if ($this->quoteDetails->removeElement($quoteDetail)) {
            // set the owning side to null (unless already changed)
            if ($quoteDetail->getQuote() === $this) {
                $quoteDetail->setQuote(null);
            }
        }

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }
}
