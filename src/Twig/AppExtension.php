<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class AppExtension
 * @package App\Twig
 * @author Olivier Brassart <obteknik2007@gmail.com>
 */
class AppExtension extends AbstractExtension
{

    public function getFilters()
    {
        return array(
            new TwigFilter('substr', array($this, 'substrFunction')),
        );
    }

    /**
     * Réduit la taille d'un texte de la longueur indiquée et ajoute '...' à la fin
     * @param string $text
     * @param $length
     * @return string
     */
    public function substrFunction(string $text, $length) : string
    {

        return substr($text, 0, $length).'...';
    }

}