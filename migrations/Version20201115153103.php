<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201115153103 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_invoice (product_id INT NOT NULL, invoice_id INT NOT NULL, INDEX IDX_41BC0CF44584665A (product_id), INDEX IDX_41BC0CF42989F1FD (invoice_id), PRIMARY KEY(product_id, invoice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_invoice ADD CONSTRAINT FK_41BC0CF44584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_invoice ADD CONSTRAINT FK_41BC0CF42989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD2989F1FD');
        $this->addSql('DROP INDEX IDX_D34A04AD2989F1FD ON product');
        $this->addSql('ALTER TABLE product DROP invoice_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE product_invoice');
        $this->addSql('ALTER TABLE product ADD invoice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD2989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD2989F1FD ON product (invoice_id)');
    }
}
