<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201114104830 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE invoice ADD organization_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_9065174432C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('CREATE INDEX IDX_9065174432C8A3DE ON invoice (organization_id)');
        $this->addSql('ALTER TABLE quote ADD organization_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quote ADD CONSTRAINT FK_6B71CBF432C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('CREATE INDEX IDX_6B71CBF432C8A3DE ON quote (organization_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_9065174432C8A3DE');
        $this->addSql('DROP INDEX IDX_9065174432C8A3DE ON invoice');
        $this->addSql('ALTER TABLE invoice DROP organization_id');
        $this->addSql('ALTER TABLE quote DROP FOREIGN KEY FK_6B71CBF432C8A3DE');
        $this->addSql('DROP INDEX IDX_6B71CBF432C8A3DE ON quote');
        $this->addSql('ALTER TABLE quote DROP organization_id');
    }
}
